Django==1.11
django-ckeditor==5.6.1
django-imagekit==4.0.2
Pillow==5.2.0
Khayyam==3.0.17
