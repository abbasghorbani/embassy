from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^home$', views.home, name='home'),
    url(r'^post/(?P<hashcode>[0-9]+)$', views.show_post, name='show-post'),
    url(r'^profile/(?P<profile_id>[0-9]+)$', views.show_profile, name='show-profile'),
    url(r'^showAll/(?P<content_type>[a-zA-Z]+)/(?P<box_id>[0-9]+)$', views.show_all, name='show-all'),
    url(r'gallery/video$', views.video_albums, name='video-albums'),
    url(r'gallery/photo$', views.photo_albums, name='photo-albums'),
    url(r'^videoGallery/(?P<album_id>[0-9]+)/$', views.video_album, name='video-album'),
    url(r'^photoGallery/(?P<album_id>[0-9]+)/$', views.photo_album, name='photo-album'),
]
