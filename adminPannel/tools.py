# -*- coding: utf-8 -*-

from django.conf import settings


def media_abs_url(url):
    if url.startswith('http://'):
        new_url = url
    else:
        new_url = settings.SITE_URL + url
    return new_url
