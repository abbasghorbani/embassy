from django import template
import datetime
import time
import khayyam
from adminPannel.models import Category, Profile


register = template.Library()


def print_timestamp(timestamp):
    try:
        ts = float(timestamp)
    except ValueError:
        return None
    return datetime.datetime.fromtimestamp(ts)


def jalali_date(date):
    """Converts Date into JalaliDate"""
    timestamp = time.mktime(datetime.datetime.timetuple(date))
    jalali_date = khayyam.JalaliDate.fromtimestamp(timestamp)
    return str(jalali_date)


@register.simple_tag
def category_list(language):
    cates = Category.objects.filter(parent_id=None, language=language, is_hidden=False).order_by('sort').all()
    cats = []
    data_dict = {}
    for cat in cates:
        cats.append(cat)

    for cat in cats:
        data = Category.objects.filter(parent=cat, language=language, is_hidden=False).order_by('sort').all()
        if cat.parent is None:
            data_dict[cat.id] = cat.get_json()
            data_dict[cat.id]["items"] = []
        for d in data:
            data_dict[cat.id]["items"].append(d.get_json())
            cats.append(d)
    return data_dict


@register.simple_tag
def get_profiles(language):
    profiles = Profile.objects.filter(language=language).all()
    profile_list = []
    for profile in profiles:
        profile_list.append(profile.get_json())
    return profile_list


# register.assignment_tag(category_list)
register.filter(jalali_date)
register.filter(print_timestamp)
