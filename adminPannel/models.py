# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import time
import uuid

from datetime import datetime

from django.db import models
from django.utils.translation import ugettext as _
from django.core import serializers
from django.dispatch import receiver
from django.core.validators import MaxValueValidator
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit

from tools import media_abs_url
from ckeditor.fields import RichTextField

FARSI = "fa"
ENGLISH = "en"
BENGALI = "bn"
LANGUAGE = (
    (FARSI, _('Persian')),
    (ENGLISH, _('English')),
    (BENGALI, _('Bengali')),
)


def profile_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    news_prefix = "profile/"
    return '/'.join([news_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def albums_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    album_prefix = "albums/"
    return '/'.join([album_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def box_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    box_prefix = "box/"
    return '/'.join([box_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def post_file_name(instance, filename):
    new_filename = str(time.time()).replace('.', '')
    fileext = os.path.splitext(filename)[1]
    if not fileext:
        fileext = '.jpg'

    filestr = new_filename + fileext
    d = datetime.now()
    post_prefix = "post/"
    return '/'.join([post_prefix, str(d.year), str(d.month), str(d.day), str(filestr)])


def validate_nonzero(value):
    if value == 0:
        raise ValidationError(_('Quantity %(value)s is not allowed'), params={'value': value})


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.jpeg', '.mp4']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))


class Album(models.Model):
    class Meta:
        db_table = 'album'
        verbose_name_plural = _("Albums")
        verbose_name = _("album")

    title = models.CharField(max_length=256, verbose_name=_("Title"), unique=True)
    description = RichTextField(verbose_name=_("description"), default="")
    thumb = ProcessedImageField(upload_to=albums_file_name, processors=[ResizeToFit(300)], format='JPEG', options={'quality': 90})
    is_visible = models.BooleanField(default=True, verbose_name=_("Is visible"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    alt = models.CharField(max_length=255, default=uuid.uuid4)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = datetime.now()
        super(Album, self).save(*args, **kwargs)


class AlbumFile(models.Model):
    class Meta:
        db_table = 'AlbumFile'
        verbose_name_plural = _("Album File")
        verbose_name = _("Album File")
    file = models.FileField(upload_to=albums_file_name, validators=[validate_file_extension], verbose_name=_("File"))
    thumb = ProcessedImageField(upload_to=albums_file_name, processors=[ResizeToFit(300)], format='JPEG', options={'quality': 80})
    album = models.ForeignKey(Album, on_delete=models.PROTECT)
    alt = models.CharField(max_length=255, default=uuid.uuid4)
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    is_video = models.BooleanField(verbose_name=_("Is video"), default=False)

    def save(self, *args, **kwargs):
        if self.id:
            ext = self.file.url.split(".")
            if ext[1] == "mp4":
                self.is_video = True
        super(AlbumFile, self).save(*args, **kwargs)


class Profile(models.Model):
    class Meta:
        db_table = 'profile'
        verbose_name_plural = _("Profiles")
        verbose_name = _("profile")

    name = models.CharField(max_length=255, verbose_name=_("Name"))
    position = models.CharField(max_length=255, verbose_name=_("Position"))
    image = ProcessedImageField(upload_to=profile_file_name, processors=[ResizeToFit(106)], format='JPEG', options={'quality': 70})
    description = RichTextField(verbose_name=_("Description"))
    language = models.CharField(verbose_name=_('Language'), choices=LANGUAGE, default=ENGLISH, max_length=5)

    def get_json(self, is_child=False):
            serialized_obj = serializers.serialize('python', [self])
            data = serialized_obj[0]['fields']
            data['id'] = serialized_obj[0]['pk']

            for k in data.keys():
                if data[k] is None:
                    data[k] = ""
            return dict(data)


class Box(models.Model):
    class Meta:
        db_table = 'box'
        verbose_name_plural = _("Boxes")
        verbose_name = _("Box")
        unique_together = ('sort', 'language',)

    image = ProcessedImageField(upload_to=box_file_name, processors=[ResizeToFit(50)], format='JPEG', options={'quality': 70})
    title = models.CharField(max_length=256, verbose_name=_("Title"))
    sort = models.PositiveIntegerField(verbose_name=_("Sort"), default=1, validators=[MaxValueValidator(1000000), validate_nonzero])
    language = models.CharField(verbose_name=_('Language'), choices=LANGUAGE, default=ENGLISH, max_length=5)

    def __unicode__(self):
        return self.title

    def get_json(self, is_child=False):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        if self.image:
            data['image'] = "{}".format(self.image.url)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""
        return dict(data)

    @classmethod
    def get_all_boxes(cls, language):
        return cls.objects.only('id', 'image', 'title').filter(language=language).order_by('sort')


class Category(models.Model):
    class Meta:
        db_table = 'category'
        verbose_name_plural = _("categories")
        verbose_name = _("category")

    title = models.CharField(max_length=256, verbose_name=_("Title"))
    parent = models.ForeignKey("self", verbose_name=_("Parent"), blank=True, null=True)
    url = models.CharField(max_length=256, verbose_name=_("URL"), blank=True, null=True)
    language = models.CharField(verbose_name=_('Language'), choices=LANGUAGE, default=ENGLISH, max_length=5)
    is_hidden = models.BooleanField(verbose_name=_("Is hidden"), default=False)
    sort = models.PositiveIntegerField(verbose_name=_("Sort"), default=1, validators=[MaxValueValidator(1000000), validate_nonzero])

    def __unicode__(self):
        return self.title

    def get_json(self, is_child=False):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']

        for k in data.keys():
            if data[k] is None:
                data[k] = ""
        return dict(data)

    def save(self, *args, **kwargs):

        super(Category, self).save(*args, **kwargs)
        


class Post(models.Model):
    class Meta:
        db_table = 'post'
        verbose_name_plural = _("Posts")
        verbose_name = _("post")

    title = RichTextField(verbose_name=_("Title"))
    text = RichTextField(verbose_name=_("Text"))
    hashcode = models.CharField(unique=True, max_length=256, verbose_name=_("Hashcode"), default=datetime.now().strftime("%s%f"))
    # box = models.ForeignKey(Box, verbose_name=_("Box ID"))
    object_id = models.PositiveIntegerField(verbose_name=_("Object ID"), blank=True, null=True)
    content_type = models.ForeignKey(ContentType, verbose_name=_("Type"), blank=True, null=True)
    image = ProcessedImageField(upload_to=post_file_name, processors=[ResizeToFit(600, 450)], format='JPEG', options={'quality': 90}, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    modified = models.DateTimeField(auto_now_add=True, verbose_name=_("Modified"))
    language = models.CharField(verbose_name=_('Language'), choices=LANGUAGE, default=ENGLISH, max_length=5)

    def get_json(self):
        serialized_obj = serializers.serialize('python', [self])
        data = serialized_obj[0]['fields']
        data['id'] = serialized_obj[0]['pk']
        if self.image:
            data['image'] = media_abs_url(self.image.url)
        data['url'] = "/post/{}".format(self.hashcode)

        for k in data.keys():
            if data[k] is None:
                data[k] = ""

        return dict(data)

    def save(self, *args, **kwargs):
        if not self.id:
            self.hashcode = datetime.now().strftime("%s%f")
        if self.id:
            self.modified = datetime.now()
        super(Post, self).save(*args, **kwargs)


@receiver(models.signals.post_save, sender=Category)
def update_category_url(sender, instance, **kwargs):
    if kwargs.get("created", False):
        if instance.parent is None:
            if instance.title.lower() in ["home", "صفحه نخست"]:
                instance.url = "/{}".format("home")
            elif instance.title.lower() in ["contact us", "ارتباط با ما"]:
                instance.url = "/showAll/category/{}".format(instance.id)
            else:
                instance.url = None
        elif instance.title.lower() in ["video", "فیلم"]:
            instance.url = "/gallery/{}".format("video")
        elif instance.title.lower() in ["photo", "عکس"]:
            instance.url = "/gallery/{}".format("photo")
        else:
            instance.url = "/showAll/category/{}".format(instance.id)
        
        instance.save()

# Album
@receiver(models.signals.post_delete, sender=Album)
def album_auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.thumb:
        if os.path.isfile(instance.thumb.path):
            os.remove(instance.thumb.path)


@receiver(models.signals.pre_save, sender=Album)
def album_auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).thumb
    except sender.DoesNotExist:
        return False

    new_file = instance.thumb
    if old_file and not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


# Album Image
@receiver(models.signals.post_delete, sender=AlbumFile)
def image_auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.thumb:
        if os.path.isfile(instance.thumb.path):
            os.remove(instance.thumb.path)

    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


@receiver(models.signals.pre_save, sender=AlbumFile)
def image_auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        file1 = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        return False
    old_thumb = file1.thumb
    old_image = file1.file

    new_thumb = instance.thumb
    if old_thumb and not old_thumb == new_thumb:
        if os.path.isfile(old_thumb.path):
            os.remove(old_thumb.path)

    new_image = instance.file
    if old_image and not old_image == new_image:
        if os.path.isfile(old_image.path):
            os.remove(old_image.path)
