# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.urls import reverse

from models import Box, Post, Category, Profile, AlbumFile, Album


def home(request):
    lang = request.LANGUAGE_CODE
    boxes = Box.get_all_boxes(language=lang)
    data = {}
    cnt_box = 0
    content_type = ContentType.objects.only('id').get(app_label="adminPannel", model="box")
    for box in boxes:
        cnt_box += 1
        box_key = str(box.sort)
        data[box_key] = box.get_json()
        data[box_key]["items"] = []
        items = Post.objects.filter(content_type_id=content_type.id, object_id=box.id, language=lang)[0:6]
        for item in items:
            data[box_key]["items"].append(item.get_json())

    return render(request, 'home.html', {'data': data, "content_type": "box"})


def show_post(request, hashcode):
    lang = request.LANGUAGE_CODE
    try:
        post = Post.objects.get(hashcode=hashcode, language=lang)
    except Exception:
        post = None
        back_title = None

    if post:
        if post.content_type.model == "box":
            box = Box.objects.only('title').get(id=post.object_id)
            back_title = box.title
        else:
            cat = Category.objects.only('title').get(id=post.object_id, is_hidden=False)
            back_title = cat.title
    return render(request, 'show_post.html', {'post': post, 'back_title': back_title})


def show_all(request, content_type, box_id):
    lang = request.LANGUAGE_CODE
    box = None
    posts = None
    if content_type == "category":
        try:
            box = Category.objects.get(id=box_id, language=lang, is_hidden=False)
        except Exception:
            pass
    else:
        try:
            box = Box.objects.get(id=box_id, language=lang)
        except Exception:
            pass

    if box:
        content_type = ContentType.objects.only('id').get(app_label="adminPannel", model=content_type.lower())
        posts_obj = Post.objects.filter(object_id=box_id, language=lang, content_type_id=content_type.id).order_by('-id')[0:20]
        if len(posts_obj) == 1:
            return HttpResponseRedirect(reverse('show-post', args=[str(posts_obj[0].hashcode)]))
        posts = []
        for post in posts_obj:
            posts.append(post.get_json())

    return render(request, 'show_more.html', {'posts': posts, 'box': box})


def show_profile(request, profile_id):
    lang = request.LANGUAGE_CODE
    profile = None
    try:
        profile = Profile.objects.get(id=profile_id, language=lang)
        profile = profile.get_json()
    except Exception:
        pass
    return render(request, 'show_profile.html', {'profile': profile})


def video_albums(request):
    albume_ids = AlbumFile.objects.filter(is_video=True).values_list('album', flat=True).distinct()
    albums = Album.objects.filter(id__in=albume_ids)
    album_list = []
    for album in albums:
        album_list.append(album)
    return render(request, 'show_albumes.html', {'albumes': album_list, "title": "Video Gallery", "type": "video"})


def video_album(request, album_id):
    files = AlbumFile.objects.filter(is_video=True, album_id=album_id).all()
    return render(request, 'gallery.html', {'files': files})


def photo_albums(request):
    albume_ids = AlbumFile.objects.filter(is_video=False).values_list('album', flat=True).distinct()
    albums = Album.objects.filter(id__in=albume_ids)
    album_list = []
    for album in albums:
        album_list.append(album)
    return render(request, 'show_albumes.html', {'albumes': album_list, "title": "Photo Gallery", "type": "photo"})


def photo_album(request, album_id):
    files = AlbumFile.objects.filter(is_video=False, album_id=album_id).all()
    return render(request, 'gallery.html', {'files': files})
