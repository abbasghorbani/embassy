# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-08-23 13:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adminPannel', '0010_auto_20180823_1303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='album',
            name='user',
        ),
        migrations.AddField(
            model_name='albumfile',
            name='is_video',
            field=models.BooleanField(default=False, verbose_name='Is video'),
        ),
        migrations.AlterField(
            model_name='post',
            name='hashcode',
            field=models.CharField(default=b'1535013833270011', max_length=256, unique=True, verbose_name='Hashcode'),
        ),
    ]
