# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.contenttypes.models import ContentType

from models import Album, AlbumFile, Box, Post, Category, Profile
from django.db.models import Q
from django import forms


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'thumb', 'is_visible', 'created')
    search_fields = ['created', 'title_en']
    list_filter = ('created',)


class AlbumFileAdmin(admin.ModelAdmin):
    list_display = ('id', 'file', 'thumb', 'album', 'created')
    search_fields = ['created']
    raw_id_fields = ("album",)
    list_filter = ('created',)


class BoxAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image', "language")
    search_fields = ['title']


class CustomUserChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.title


class PostForm(forms.ModelForm):
    class Meta(object):
        model = Post
        fields = ["language", "title", "text", "hashcode", "content_type", "object_id", "image"]

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['content_type'] = forms.ChoiceField(choices=ContentType.objects.filter(Q(app_label="adminPannel", model="box") | Q(app_label="adminPannel", model="category")).values_list('id', 'model'), initial="category")

    def clean_content_type(self):
        try:
            self.cleaned_data['content_type'] = ContentType.objects.get(id=self.cleaned_data['content_type'])
        except Exception:
            raise forms.ValidationError("This type is not found ")
        return self.cleaned_data['content_type']

    def clean(self, *args, **kwargs):
        cleaned_data = super(PostForm, self).clean(*args, **kwargs)
        object_id = cleaned_data.get('object_id', 0)
        model = cleaned_data['content_type'].model
        if model == "box":
            count = Box.objects.filter(id=object_id).count()
            if count == 0:
                raise forms.ValidationError({'object_id': ["There is not found box with id {}".format(object_id)]})
        elif model == "category":
            count = Category.objects.filter(id=object_id).count()
            if count == 0:
                raise forms.ValidationError({'object_id': ["There is not found category with id {}".format(object_id)]})
        else:
            raise forms.ValidationError("This type is not found ")
        return cleaned_data


class PostAdmin(admin.ModelAdmin):
    form = PostForm
    list_display = ('id', 'created', 'title', 'content_type', 'object_id', 'image', 'language')
    search_fields = ['hashcode']
    list_filter = ('created',)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'language', 'parent', 'url', 'id', 'sort')
    search_fields = ['title']
    list_filter = ('language',)
    raw_id_fields = ("parent",)
    fields = ('title', 'language', 'parent', 'sort', 'is_hidden')


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('name', 'language', 'image', 'position')
    list_filter = ('language',)


admin.site.register(Album, AlbumAdmin)
admin.site.register(AlbumFile, AlbumFileAdmin)
admin.site.register(Box, BoxAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Profile, ProfileAdmin)
